#include "ProgramB.h"


vector<int> calc(const int& world_size, vector<int>& v) {
	int remainder = v.size() % world_size;
	if (remainder > 0) {
		for (unsigned int i = remainder;i > 0;--i) {
			v.push_back(0);
		}
	}
	return v;
}

inline const int sendToNextProcess(const int& world_size, const int& id) {
	return (id == 0 ? world_size : id) - 1;
}

inline const int recvFromPreviousProcess(const int& world_size, const int& id) {
	return (id == world_size - 1 ? -1 : id) + 1;
}

void program_b(const int& id, const int& world_size) {
	if (world_size == 1) {
		return;
	}
	double time_start = MPI_Wtime(), time_end;

	vector<int> arr;
	int vector_size = 0;
	switch (id)
	{
	case 0:
		cout << "Vector size : ";
		cin >> vector_size;
		arr = read_from_file_vector(vector_size);
		arr = calc(world_size, arr);
		vector_size = arr.size();
		MPI_Bcast(&vector_size, 1, MPI_INT, ROOT, MPI_COMM_WORLD);
		break;
	default:
		MPI_Bcast(&vector_size, 1, MPI_INT, ROOT, MPI_COMM_WORLD);
		DEBUG(id, world_size, " vector_size ", vector_size);
		arr = vector<int>(vector_size);//init array with the max nr of elems;
		break;
	}

	int* recv = new int[vector_size];
	int size = vector_size / world_size;//elements / processor
	MPI_Scatter(&arr[0], size, MPI_INT, recv, size, MPI_INT, ROOT, MPI_COMM_WORLD);

	int localSum = 0;
	for (unsigned int i = 0;i < size;++i) {
		localSum += recv[i];
		//DEBUG(id, world_size, " got elem ", recv[i]);
	}
	DEBUG(id, world_size, " local sum ", localSum);

	MPI_Status status;
	int result;
	switch (id)
	{
	case 0:
		MPI_Send(&localSum, 1, MPI_INT, sendToNextProcess(world_size, id), MPI_TAG_UB, MPI_COMM_WORLD);
		MPI_Recv(&result, 1, MPI_INT, recvFromPreviousProcess(world_size, id), MPI_TAG_UB, MPI_COMM_WORLD, &status);
		cout << "Global sum is " << result << endl;
		time_end = MPI_Wtime();
		cout << "Elapsed time is " << time_end - time_start << endl;
		break;
	default:
		MPI_Recv(&result, 1, MPI_INT, recvFromPreviousProcess(world_size, id), MPI_TAG_UB, MPI_COMM_WORLD, &status);
		localSum += result;
		MPI_Send(&localSum, 1, MPI_INT, sendToNextProcess(world_size, id), MPI_TAG_UB, MPI_COMM_WORLD);
		break;
	}

}