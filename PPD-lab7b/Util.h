#pragma once
#ifndef UTIL_H
#define UTIL_H

#include <iostream>
#include <mpi.h>
#include <vector>
#include <fstream>
#include <string>
using namespace std;

#define ROOT 0
#if 0
#define DEBUG(id, world_size,msg,data) cout << "I'm process with id " << id << " out of " << world_size << msg << data << endl;
#endif // 0
#if 1
#define DEBUG(id, world_size,msg,data) void(0);
#endif // 0


#define FILE_NAME "vector.txt"
vector<int> read_from_file_vector(int size); //function definition

#endif //UTIL
