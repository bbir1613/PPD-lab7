#include "Util.h"

vector<int> read_from_file_vector(int size) {
	vector<int> v;
	ifstream myfile(FILE_NAME);
	int k = 0;

	if (myfile.is_open())
	{
		string line;
		while (getline(myfile, line) && size > 0)
		{
			//cout << "Line is " << line << endl;
			v.push_back(stoi(line));
			--size;
		}
		myfile.close();
	}
	else { 
		cout << "Unable to open file " << endl;
		throw exception("Unable to open file"); 
	}

	return v;
}