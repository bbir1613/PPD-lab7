#include "ProgramA.h"

chunk getStartEndById(const int& id, const int& world_size, const int& vector_size) {
	chunk chunk;
	int nrOfElementsPerProcess = vector_size / world_size;
	if (id == world_size - 1) {
		chunk.start = id * nrOfElementsPerProcess;
		chunk.end = vector_size;
	}
	else {
		chunk.start = id * nrOfElementsPerProcess;
		chunk.end = (id + 1)*nrOfElementsPerProcess;
	}
	return chunk;
}

void program_a(const int& id, const int& world_size) {
	if (world_size == 1) {
		return;
	}
	double time_start = MPI_Wtime(), time_end;
	int vector_size;
	vector<int> vector;

	if (id == 0) {
		cout << "Vector size : ";
		cin >> vector_size;
	}

	MPI_Bcast(&vector_size, 1, MPI_INT, ROOT, MPI_COMM_WORLD);

	DEBUG(id, world_size, " vector_size ", vector_size);
	vector = read_from_file_vector(vector_size);
	DEBUG(id, world_size, " vector size after initialization ", vector.size());

	int localSum = 0, nrOfElementsPerProcess = vector_size / world_size, start, end;

	chunk chunk = getStartEndById(id,world_size,vector.size());
	DEBUG(id, world_size, " chunk start ", chunk.start);
	DEBUG(id, world_size, " chunk end ", chunk.end);

	for (unsigned int i = chunk.start;i < chunk.end;++i) {
		localSum+=vector[i];
	}

	DEBUG(id, world_size, " local sum is ", localSum);
	int globalSum = 0;
	MPI_Reduce(&localSum, &globalSum, 1, MPI_INT, MPI_SUM, ROOT, MPI_COMM_WORLD);
	if (id == ROOT) {
		DEBUG(id, world_size, " global sum ", globalSum);
		cout << " Global sum is :" << globalSum << endl;
		time_end = MPI_Wtime();
		cout << "Elapsed time is " << time_end - time_start << endl;
	}
}