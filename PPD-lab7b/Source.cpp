#include "Source.h"
#include "ProgramA.h"
#include "ProgramB.h"
#include "Util.h"

int main(int argc, char** argv) {
	MPI_Init(&argc, &argv);
	int nrOfProcesses, processId;

	MPI_Comm_size(MPI_COMM_WORLD, &nrOfProcesses);//get nr of processes
	MPI_Comm_rank(MPI_COMM_WORLD, &processId);//get process id

#if PROGRAM == 0 
	program_a(processId, nrOfProcesses);
#endif
#if PROGRAM == 1 
	program_b(processId, nrOfProcesses);
#endif
#if PROGRAM == 2
	program_a(processId, nrOfProcesses);
	program_b(processId, nrOfProcesses);
#endif
	MPI_Finalize();
	return 0;
}